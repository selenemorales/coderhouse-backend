const fs = require("fs");

class ProductManager {
  constructor() {
    this.products = [];
    this.path = "../coderhouse-backend/productsFile";
  }

  // Methods of my class...

  async addProduct(title, description, price, thumbnail, code, stock) {
    /* Params ~>
      title: Product name
      description: Product description
      price: Price of the product
      thumbnail: Image path
      code: Identifier code
      stock: Number of pieces available
      */

    // Verify that all dtaa is complete and is different from null or undefined since all are mandatory
    if (!title || !description || !price || !thumbnail || !code || !stock) {
      console.error("Product could not be added, all fields are required.");
      return;
    }
        
    // Check that the product code exists
    const codeExists = this.products.some((product) => product.code === code);
    if (codeExists) {
      console.error("Error, the product code already exists.");
      return;
    }

    // After verifying that all the fields are there and that the product code does not previously exist, we add the product!

    const newProductId = this.products.length + 1; // ~> Define a unique and auto-incrementing Id for the product

    const newProduct = {
      id: newProductId,
      code,
      title,
      description,
      price,
      thumbnail,
      stock,
    };

    this.products.push(newProduct); // ~>  Add the product to my products array
    await fs.promises.writeFile(this.path, JSON.stringify(this.products)); // ~> Rewrite the products file to keep it updated
    console.log("The product was added successfully.");
  }

  // Delete a specific product
  async deleteProduct(productId) {
    await this.getProducts(); // ~> Get the products json from the file.

    let productExists = this.products.some(
      (product) => product.id === productId
    );
    if (productExists) {
      this.products = this.products.filter(
        (product) => product.id !== productId
      ); // ~> Obtain an array in which all its elements meet the stipulated condition.
      await fs.promises
        .writeFile(this.path, JSON.stringify(this.products))
        .catch((err) => console.error(err));
      console.log("The product was successfully removed.");
    } else {
      console.error("The product with the indicated id does not exist.");
    }
  }

  // Gets all products created up to that point
  async getProducts() {
    console.log("getProducts");
    const fileProducts = await fs.promises.readFile(this.path, "utf-8");
    console.log("fileProducts", fileProducts);
    this.products = fileProducts ? JSON.parse(fileProducts) : [];
    return this.products;
  }

  // Search the array for the product that matches the id and returns it
  async getProductById(id) {
    await this.getProducts(); // ~> Get the products json from the file.
    const productFound = this.products.find((product) => product.id === id);
    if (!productFound) return console.error("Not found.");
    return productFound;
  }

  // Update a specific product
  async updateProduct(productId, updateJson) {
    await this.getProducts(); // ~> Get the products json from the file.
    const productIndex = this.products.findIndex(
      (product) => product.id === productId
    ); // ~> I get the index of the product that has the indicated id

    // I modify the element :
    if (productIndex !== -1) {
      this.products[productIndex] = {
        ...this.products[productIndex], // ~> The object as it is.
        ...updateJson, // ~> I add the new properties json.
      };

      await fs.promises.writeFile(this.path, JSON.stringify(this.products));
      console.log(`The product ${productId} was successfully updated.`);
    } else {
      console.log(`The product ${productId} was not found.`);
    }
  }
}

// Testing class
// let prod = new ProductManager();
// (async () => {
// let products0 = await prod.getProducts();

// console.log("products0", products0);

// await prod.addProduct(
//   "producto prueba",
//   "Este es un producto prueba",
//   200,
//   "Sin imagen",
//   "abc123",
//   25
// );

// let products1 = await prod.getProducts();
// console.log("products1", products1);

// let productById1 = await prod.getProductById(1);
// console.log("productById1", productById1);

// let productById2 = prod.getProductById(2);
// console.log("productById2", productById2);

// await prod.updateProduct(8, {
//   newProperty: "Nueva propiedad",
//   title: "Actualice el titulo",
// });

// await prod.deleteProduct(1)

// await prod.deleteProduct(6)
// })();
