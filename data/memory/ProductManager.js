class ProductManager0 {
    constructor() {
      this.products = [];
    }
  
    // Methods of my class...
  
    addProduct(title, description, price, thumbnail, code, stock) {
      /* Params ~>
      title: Product name
      description: Product description
      price: Price of the product
      thumbnail: Image path
      code: Identifier code
      stock: Number of pieces available
      */
  
      // Verify that all dtaa is complete and is different from null or undefined since all are mandatory
      if (!title || !description || !price || !thumbnail || !code || !stock) {
        console.error(
          "Product could not be added, all fields are required."
        );
        return;
      }
  
      // Check that the product code exists
      const codeExists = this.products.some((product) => product.code === code);
      if (codeExists) {
        console.error("Error, the product code already exists.");
        return;
      }
  
      // After verifying that all the fields are there and that the product code does not previously exist, we add the product!
  
      const newProductId = this.products.length + 1; // ~> Define a unique and auto-incrementing Id for the product
  
      const newProduct = {
        id: newProductId,
        code,
        title,
        description,
        price,
        thumbnail,
        stock,
      };
  
      this.products.push(newProduct); // ~>  Add the product to my products array
      console.log('The product was added successfully.')
    }
  
    // Gets all products created up to that point
    getProducts() {
      return this.products;
    }
  
    // Search the array for the product that matches the id and returns it
    getProductById(id) {
      const productFound = this.products.find((product) => product.id === id);
      if (!productFound) return console.error("Not found.");
      return productFound;
    }
  }